# Template for flutter apps

## How to use this template
### Auto CICD
Provide the variables values in the ```gitlab-ci.yml```.

### Local development using Docker
Provide the variables values and the image url (project's registry)in the```docker-compose.yml``` file.
  
## Play store auto deployment
To send an app version to the play store, tag the code using [semantic versioning](https://semver.org/) followed by a `+` and the lane you want to push the app version. 

e.g. ```1.2.3+alpha```