echo "${ANDROID_APP_KEYSTORE_HEX}" | xxd -r -p - > "/build/${CI_APP_NAME}.keystore"
echo "${ANDROID_APP_SECRET_64}" | base64 -d > "/build/${CI_APP_NAME}_secret.json"
/bin/bash